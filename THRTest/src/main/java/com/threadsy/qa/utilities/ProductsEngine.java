package com.threadsy.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.xml.sax.Locator;

import com.aventstack.extentreports.Status;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ProductsEngine {
	public WebDriver driver;
	public static WebElement ele;
	public static Workbook book;
	public static Sheet sheet;
	public Initial base;
	public final String SCENARIO_SHEET_PATH = System.getProperty("user.dir") + "/HybridTestCase.xls";
	public static Properties prop;
	int f = 0;

	public void startProductExecution(WebDriver driver, String[] data, String sheetname, com.aventstack.extentreports.ExtentTest test) throws Exception, IOException {
		prop = Initial.initProperties();
		//Reading the Hybrid test Case file
		File file = new File(SCENARIO_SHEET_PATH);
		Workbook book = WorkbookFactory.create(file);
		Sheet sheet = (org.apache.poi.ss.usermodel.Sheet) book.getSheet(sheetname);
		int col = 0, j = 0;
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				//logging the executed steps in Extent Report
				test.log(Status.INFO, sheet.getRow(i).getCell(col).toString().trim());
				
				String locatorType = sheet.getRow(i).getCell(col + 1).toString().trim();
				String action = sheet.getRow(i).getCell(col + 2).toString().trim();
				String locatorValue = sheet.getRow(i).getCell(col + 3).toString().trim();
			
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("waitVisible")){
					locatorValue = prop.getProperty(locatorValue)+data[j]+"')]";					
				}
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("waitPresent")){
					locatorValue = prop.getProperty(locatorValue)+data[j]+"')]";					
				}
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("click")){
					String[] eachXpath = {""};
					//if there are multiple Product Elements in data String - split
					if(locatorValue.contains(",")) 
						eachXpath = locatorValue.split(",");
					locatorValue = prop.getProperty(eachXpath[0])+data[j]+prop.getProperty(eachXpath[1]);
					j++;
				}
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("JSsendkeys")){
					String[] eachXpath = {""};
					//if there are multiple Product Elements in data String - split
					if(locatorValue.contains(",")) 
						eachXpath = locatorValue.split(",");
					locatorValue = prop.getProperty(eachXpath[0])+data[j]+prop.getProperty(eachXpath[1])+data[j+1]+prop.getProperty(eachXpath[2]);
					j++;
					System.out.println("-dynamic path-SendKeys-Qnty--row no.--"+i+"---data[j]--->"+data[j]);
					j++;
				}
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("explicitwait")){
					locatorValue = prop.getProperty(locatorValue)+data[j]+"')]";
					j++;
				}
				if(locatorType.equalsIgnoreCase("dynamicxpath") && action.equalsIgnoreCase("wait&click")){
					locatorValue = prop.getProperty(locatorValue)+data[j]+"')]";
					j++;
				}
				if (action.equalsIgnoreCase("pagination")) {
					System.out.println("Inside pagination---data[j]--->"+data[j]);
					ProductsEngine.paginationSelect(driver, data[j]);
					j++;
				} else if (action.equalsIgnoreCase("openBrowser")) {
					ProductsEngine.openBrowser(driver);
					
				}else if (action.equalsIgnoreCase("sendkeys")) {			
					ProductsEngine.sendKey(driver, locatorType, locatorValue, data[j]);
					j++;
				}else if(action.equalsIgnoreCase("switchFrame")){
					ProductsEngine.SwitchToIframe(driver, locatorType, locatorValue);
					
				}else if(action.equalsIgnoreCase("switchBack")){
					ProductsEngine.switchBack(driver, locatorType, locatorValue);
					
				}else if (action.equalsIgnoreCase("enterkey")) {
					j--;
					ProductsEngine.enterKey(driver, locatorType, locatorValue);
					
				}else if (action.equalsIgnoreCase("optsendkeys")) {			
					//Checks for element if present enter data else skip step
					try {
						if(isElementPresent(driver,prop.getProperty(locatorValue)))
							ProductsEngine.sendKey(driver, locatorType, locatorValue, data[j]);
						} catch (NoSuchElementException exp) {
						exp.printStackTrace();
					}
				}
				else if (action.equalsIgnoreCase("click")) {
					ProductsEngine.click(driver, locatorType, locatorValue);
				} 
				else if (action.equalsIgnoreCase("click&send")) {
					ProductsEngine.clickAndSend(driver, locatorType, locatorValue,data[j]);
					j++;
				}else if (action.equalsIgnoreCase("explicitwait")) {
					ProductsEngine.explicitWaitAndClick(driver, locatorType, locatorValue);					
				}
				else if (action.equalsIgnoreCase("wait&click")) {
					ProductsEngine.waitAndClick(driver, locatorType, locatorValue);					
				} 
				else if (action.equalsIgnoreCase("wait&click2")) {
					ProductsEngine.waitAndClick(driver, locatorType, locatorValue);
					j++;
				} else if (action.equalsIgnoreCase("simpleWait&Click")) {
					ProductsEngine.simpleWaitAndClick(driver, locatorType, locatorValue);
				} else if (action.equalsIgnoreCase("clickaction")) {
					ProductsEngine.mouseClickAction(driver, locatorType, locatorValue);
				}else if (action.equalsIgnoreCase("clickJS")) {
					ProductsEngine.JSClick(driver, locatorType, locatorValue);
				}else if (action.equalsIgnoreCase("JSsendkeys")) {
					ProductsEngine.JSsendkeys(driver, locatorType, locatorValue, data[j]);
					j++;
				}else if (action.equalsIgnoreCase("mouseMove")) {
					ProductsEngine.mouseMove(driver, locatorType, locatorValue);
				} else if (action.equalsIgnoreCase("select")) {
					ProductsEngine.select(driver, locatorType, locatorValue, data[j]);
					j++;
				}else if (action.equalsIgnoreCase("waitVisible")) {
					ProductsEngine.waitVisiblility(driver, locatorType, locatorValue);
				}else if (action.equalsIgnoreCase("waitClickable")) {
					ProductsEngine.waitClickable(driver, locatorType, locatorValue);
				}
				else if (action.equalsIgnoreCase("waitPresent")) {
					ProductsEngine.waitForPresence(driver, locatorType, locatorValue);
				}else if (action.equalsIgnoreCase("scroll")) {
					ProductsEngine.scrollIntoView(driver, locatorType, locatorValue);
				} else if(action.equalsIgnoreCase("acceptConfirm")) {
					System.out.println("inside  accept YES alert");
					ProductsEngine.click(driver, locatorType, locatorValue);
				//	new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locatorValue)))).click();
				} else if(action.equalsIgnoreCase("denyConfirm")) {
					System.out.println("inside  accept No alert");
					try {
						if(isElementPresent(driver,prop.getProperty(locatorValue)))
							new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locatorValue)))).click();
							//ProductsEngine.click(driver, locatorType, locatorValue);							
					} catch(Exception e) {
						System.out.println("--catch");
						e.printStackTrace();
					}
				} else if(action.equalsIgnoreCase("acceptOk")) {
					try {
					System.out.println("inside accept OK alert");
					//ProductsEngine.click(driver, locatorType, locatorValue);
					new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locatorValue)))).click();
					} catch(Exception e) {
						e.printStackTrace();
					}
				} 

			} catch (Exception e) {
				//test.log(LogStatus.FAIL,test.addScreenCapture(imagePath)(capture(driver))+"Test failed");
				e.printStackTrace();
				throw(e);
			}
		}

	}

	private static void JSsendkeys(WebDriver driver2, String locatorType, String locatorValue, String data)
			throws IOException {
		// int qnty = Integer.parseInt(data);
		ele = ProductsEngine.getElement(driver2, locatorValue, locatorType);
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		js.executeScript("arguments[0].value='" + data + "'", ele);

	}

	private static void SwitchToIframe(WebDriver driver2, String locatorType, String locatorValue) throws IOException {
		// driver2.switchTo().frame(name);
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		WebDriverWait wait = new WebDriverWait(driver2, 30);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(prop.getProperty(locatorValue))));
	}

	private static void switchBack(WebDriver driver2, String locatorType, String locatorValue) throws IOException {
		// driver2.switchTo().frame(name);
		driver2.switchTo().parentFrame();
	}

	private static void enterKey(WebDriver driver2, String locatorType, String locatorValue) throws IOException {
		ele = ProductsEngine.getElement(driver2, locatorValue, locatorType);
		ele.sendKeys(Keys.ENTER);
	}

	public static void JSClick(WebDriver driver2, String locatorType, String locatorValue) throws IOException {
		ele = ProductsEngine.getElement(driver2, locatorValue, locatorType);
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		js.executeScript("arguments[0].click()", ele);
	}

	private String capture(WebDriver driver2) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void scrollIntoView(WebDriver driver2, String locatorType, String locator) throws IOException {
		Properties prop = Initial.initProperties();
		WebDriverWait wait = new WebDriverWait(driver2, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty(locator))));
		ele = ProductsEngine.getElement(driver2, locator, locatorType);
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		js.executeScript("arguments[0].scrollIntoView(true);", ele);
	}

	public static void waitClickable(WebDriver driver2, String locatorType, String locator) throws IOException {
		System.out.println("inside---Wait Clickable--->>>" + locator);
		Properties prop = Initial.initProperties();
		WebDriverWait wait = new WebDriverWait(driver2, 10);
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		else
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locator))));
	}

	public static void waitVisiblility(WebDriver driver2, String locatorType, String locator) throws IOException {
		Properties prop = Initial.initProperties();
		WebDriverWait wait = new WebDriverWait(driver2, 20);
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		else
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty(locator))));

	}

	public static void waitForPresence(WebDriver driver2, String locatorType, String locator) throws IOException {
		Properties prop = Initial.initProperties();
		WebDriverWait wait = new WebDriverWait(driver2, 30);
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		else
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty(locator))));
	}

	public static void paginationSelect(WebDriver driver, String data) throws IOException {
		Properties prop = Initial.initProperties();
		String[] eachProductElem = { "" };
		// if there are multiple Product Elements in data String - split
		System.out.println("Each Product Style is----" + data);
		Boolean isElemPresent = ProductsEngine.isElementPresent(driver,
				prop.getProperty("Product_Before_xpath") + data + prop.getProperty("Product_After_xpath"));
		if (!isElemPresent) {
			while (!isElemPresent) {
				System.out.println("Pagination .......");
				if (driver.findElements(By.xpath(prop.getProperty("Search_Pagination_xpath"))).size() != 0)
					driver.findElement(By.xpath(prop.getProperty("Search_Pagination_xpath"))).click();
				else if (driver
						.findElements(By.xpath(
								prop.getProperty("Style_before_xpath") + data + prop.getProperty("Style_after_xpath")))
						.size() != 0) {
					break;
				} else {
					System.out.println("--Terminating the Test--- Skip Exception---");
					throw new SkipException("Skipping the Test---As Product is not avaiable in search for Testing");
				}
				isElemPresent = (driver.findElements(By.xpath(
						prop.getProperty("Product_Before_xpath") + data + prop.getProperty("Product_After_xpath")))
						.size() != 0);
			}
		} else if (isElemPresent) {
			driver.findElement(
					By.xpath(prop.getProperty("Product_Before_xpath") + data + prop.getProperty("Product_After_xpath")))
					.click();
		}
	}

	public static String getText(WebDriver driver, String locatorType, String locator) throws Exception {
		ele = ProductsEngine.getElement(driver, locator, locatorType);
		String dataText = ele.getText();
		return dataText;
	}

	public static String getValue(WebDriver driver, String locatorType, String locator) throws Exception {
		ele = ProductsEngine.getElement(driver, locator, locatorType);
		String dataText = ele.getAttribute("value");
		return dataText;
	}

	public static void sendKey(WebDriver driver, String locatorType, String locator, String value) throws Exception {
		ele = ProductsEngine.getElement(driver, locator, locatorType);
		ele.sendKeys(value);
	}

	public static void waitAndClick(WebDriver driver2, String locatorType, String locatorValue) throws IOException {
		// TODO Auto-generated method stub
		WebElement waitEle;
		Properties prop = Initial.initProperties();
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			waitEle = Initial.wait_Fluent(driver2, locatorValue);
		else
			waitEle = Initial.wait_Fluent(driver2, prop.getProperty(locatorValue));
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		// js.executeScript("arguments[0].click()", waitEle);
		// waitEle.click();
		Actions action = new Actions(driver2);
		action.moveToElement(waitEle).build().perform();
		action.moveToElement(waitEle).click().perform();
	}

	public static void explicitWaitAndClick(WebDriver driver2, String locatorType, String locatorValue)
			throws IOException {
		// TODO Auto-generated method stub
		WebElement waitEle;
		Properties prop = Initial.initProperties();
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			waitEle = Initial.wait_Explicit(driver2, locatorValue, 7);
		else
			waitEle = Initial.wait_Explicit(driver2, prop.getProperty(locatorValue), 7);
		JavascriptExecutor js = ((JavascriptExecutor) driver2);
		// js.executeScript("arguments[0].click()", waitEle);
		Actions action = new Actions(driver2);
		action.moveToElement(waitEle).build().perform();
		action.moveToElement(waitEle).click().perform();

	}

	public static void simpleWaitAndClick(WebDriver driver2, String locatorType, String locatorValue)
			throws IOException {
		// TODO Auto-generated method stub
		WebElement waitEle;
		Properties prop = Initial.initProperties();
		if (locatorType.equalsIgnoreCase("dynamicxpath"))
			waitEle = Initial.waitUntilSelectOptionsPopulated(driver2, locatorValue);
		else
			waitEle = Initial.waitUntilSelectOptionsPopulated(driver2, prop.getProperty(locatorValue));
		// js.executeScript("arguments[0].click()", waitEle);
		waitEle.click();
	}

	public static void click(WebDriver driver, String locatorType, String locator) throws Exception {
		WebElement clkEle = ProductsEngine.getElement(driver, locator, locatorType);
		clkEle.click();

	}

	public static void clickAndSend(WebDriver driver, String locatorType, String locator, String data)
			throws Exception {
		Properties prop = Initial.initProperties();
		WebElement courseEle = Initial.wait_Fluent(driver, prop.getProperty(locator));
		courseEle.click();
		courseEle.sendKeys(data);
	}

	public static void mouseClickAction(WebDriver driver, String locatorType, String locator) throws Exception {
		Actions action = new Actions(driver);
		ele = ProductsEngine.getElement(driver, locator, locatorType);
		action.moveToElement(ele).click().perform();
	}

	public static void mouseMove(WebDriver driver, String locatorType, String locator) throws Exception {
		Actions act = new Actions(driver);
		ele = ProductsEngine.getElement(driver, locator, locatorType);
		act.moveToElement(ele).build().perform();
	}

	public static void select(WebDriver driver, String locatorType, String locator, String value) throws Exception {
		Properties prop = Initial.initProperties();
		Select selectedElem = new Select(driver.findElement(By.xpath(prop.getProperty(locator))));
		selectedElem.selectByVisibleText(value);
	}

	public static boolean isElementPresent(WebDriver driver, String xpathvalue) {
		return driver.findElements(By.xpath(xpathvalue)).size() != 0;
	}

	public static void datePicker(WebDriver driver, String xpath) {
		WebElement dateWidget = driver.findElement(By.xpath("//div[@id='txtreturnCalendar']/table[1]//td"));
		List<WebElement> columns = dateWidget.findElements(By.tagName("td"));

		for (int i = 0; i < columns.size(); i++) {
			String date = columns.get(i).getText();
			if (date.equals("19")) {
				columns.get(i).click();
				break;
			}
		}
	}

	public static void uploadDoc(WebDriver driver, String fileLocation) throws Exception {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
			throw (exp);
		}
	}

	private static void setClipboardData(String string) {
		// StringSelection is a class is used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

	}

	public static void rightClick(WebDriver driver, String locatorValue) throws Exception {
		Properties prop = Initial.initProperties();
		WebDriverWait wait = new WebDriverWait(driver, 70);
		WebElement elementLoc = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locatorValue))));
		Actions actions = new Actions(driver);
		actions.contextClick(elementLoc).perform();
		WebElement element = driver.findElement(By.cssSelector(".context-menu-icon-copy"));
		element.click();
	}

	public static WebElement getElement(WebDriver driver, String locator, String locatorType) throws IOException {
		Properties prop = Initial.initProperties();
		WebElement ele = null;
		if (locatorType.equalsIgnoreCase("id"))
			ele = driver.findElement(By.id(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("xpath"))
			ele = driver.findElement(By.xpath(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("name"))
			ele = driver.findElement(By.name(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("cssselector"))
			ele = driver.findElement(By.cssSelector(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("linktext"))
			ele = driver.findElement(By.linkText(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("classname"))
			ele = driver.findElement(By.className(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("tagname"))
			ele = driver.findElement(By.tagName(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("partiallinktext"))
			ele = driver.findElement(By.partialLinkText(prop.getProperty(locator)));
		else if (locatorType.equalsIgnoreCase("dynamicxpath"))
			ele = driver.findElement(By.xpath(locator));
		return ele;
	}

	public static void openBrowser(WebDriver driver) {
		driver.navigate().to("http://test.threadsy.com");
	}

}
