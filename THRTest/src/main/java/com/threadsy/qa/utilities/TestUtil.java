package com.threadsy.qa.utilities;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TestUtil {

	static String filepath=System.getProperty("user.dir")+"/";
	
	public static ArrayList<Object[]> excelReader(String sheetname, String name) {
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		System.out.println("inside Excel reader---sheet-->"+sheetname);
		try {
				File file = new File(filepath+name);
				if(!file.exists())
					Thread.sleep(3000);
		Workbook book = WorkbookFactory.create(file);
		Sheet sheet = (org.apache.poi.ss.usermodel.Sheet) book.getSheet(sheetname);
		System.out.println("created sheet"+((org.apache.poi.ss.usermodel.Sheet) sheet).getLastRowNum());
		String cell;
		int colCount = 0;
		for (int i = 1; i <= ((org.apache.poi.ss.usermodel.Sheet) sheet).getLastRowNum(); i++) {
			org.apache.poi.ss.usermodel.Row RowValue = ((org.apache.poi.ss.usermodel.Sheet) sheet).getRow(i);
			colCount = RowValue.getLastCellNum();
			ArrayList<String> row = new ArrayList<String>();
			for (int j = 0; j < colCount; j++) {
				cell = RowValue.getCell(j).toString().trim();
				row.add(cell);
			}
			data.add(row.toArray());
			System.out.println("--Single -Row " + row);
		}
		}
	 catch (IOException | InterruptedException e) {
		e.printStackTrace();
	}
		return data;
	}

	public static void captureScreenshot(WebDriver driver, String screenshotname) {
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source,
					new File("C:\\Users\\Documents\\ScreenShots\\" + screenshotname + ".png"));
			System.out.println("Screenshot Captured");
		} catch (Exception e) {
			System.out.println("Exception while taking Screenshot" + e.getMessage());
		}
	}

	public static boolean isAlertPresent(WebDriver driver) {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			return true;
		} catch (NoAlertPresentException ex) {
			return false;
		}
	}

	

	public static void excelWriter(String[] value, String sheetName) throws IOException {
		File file = new File(filepath+"ProductsData.xls");
		FileInputStream inputStream = new FileInputStream(file);
		// If it is xls file then create object of HSSFWorkbook class
		Workbook demoWorkbook = new HSSFWorkbook(inputStream);
		// Read excel sheet by sheet name
		Sheet sheet = demoWorkbook.getSheet(sheetName);
		// Get the current count of rows in excel file
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		// Get the first row from the sheet
		Row row = sheet.getRow(0);
		// Create a new row and append it at last of sheet
		Row newRow = sheet.createRow(rowCount + 1);
		// Create a loop over the cell of newly created Row
		for (int j = 0; j < row.getLastCellNum(); j++) {
			// Fill data in row
			Cell cell = newRow.createCell(j);
			cell.setCellValue(value[j]);
		}

		inputStream.close();
		// Create an object of FileOutputStream class to create write data in excel file
		FileOutputStream outputStream = new FileOutputStream(file);
		// write data in the excel file
		demoWorkbook.write(outputStream);
		// close output stream
		outputStream.close();
	}	

}
