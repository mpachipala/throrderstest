package com.threadsy.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.google.common.base.Function;

public class Initial {

	public static org.openqa.selenium.WebElement e1;
	public WebDriver driver;
	public Properties prop;
	

	public WebDriver init_driver(String browsername) {
		if (browsername.equals("chrome")) {
			/* if(prop.getProperty("headless").equals("yes")) { 
			 * ChromeOptions options =new ChromeOptions(); 
			 * options.addArguments("--headless"); 
			 * driver = new ChromeDriver(options); } else { */
			driver = new ChromeDriver();
		}
		return driver;
	}

	public static Properties initProperties() throws IOException {
		Properties prop = new Properties();
		FileInputStream Locator = new FileInputStream(
				"C:\\Work\\Workspace\\Threadsy\\THRTest\\src\\main\\resources\\locators.properties");
		FileInputStream Config = new FileInputStream(
				"C:\\Work\\Workspace\\Threadsy\\THRTest\\src\\main\\resources\\config.properties");
		prop.load(Config);
		prop.load(Locator);
		return prop;
	}

	public static WebElement wait_Fluent(WebDriver driver,String xPath) {
		WebElement clickCheckBox = null;
		try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(15))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		clickCheckBox = (WebElement) wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xPath));
			}
		});
		}
		catch(Exception e) {
			// TODO Auto-generated catch block
		    System.out.println("Fluent WAIT Element not clickable");
			e.printStackTrace();
			throw(e);
		}
		return clickCheckBox;
	}
	
	public static WebElement waitUntilSelectOptionsPopulated(WebDriver driver,String xPath) {
	/*	WebDriverWait wait = new WebDriverWait(driver,20);
		try {
			
			Function<WebDriver,Boolean> Function1 = new Function<WebDriver,Boolean>(){
				public Boolean apply(WebDriver driver) {
					return apply1(driver);
				}
			};						
			wait.until(Function1);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ctl00_BodyCph_IMGDIV")));
			 
		}*/
		try{
            new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(12))
            .pollingEvery(Duration.ofSeconds(4)).ignoring(NoSuchElementException.class).until(new Function<WebDriver,Boolean>(){
              public Boolean apply(WebDriver driver){
                  return (!(driver.findElement(By.xpath("//div[@id='ctl00_BodyCph_IMGDIV']"))).isDisplayed());                             
              }
            }
          );
      } 
		/*try {
		WebDriverWait wait=new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='ctl00_BodyCph_IMGDIV']//img")));
		//	 Thread.sleep(4000);
		} 	*/
	/*	try{
            new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
            .pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class).until(ExpectedConditions.invisibilityOfElementLocated(By.id("ctl00_BodyCph_IMGDIV")));
            	                            
      }*/ catch(Exception e) {
			System.out.println("progress bar not found");
			e.printStackTrace();
			throw(e);
		}
	//	WebDriverWait wait1 = new WebDriverWait(driver,20);
		WebElement elem = driver.findElement(By.xpath(xPath));
		return elem;
					
	}
	public static Boolean apply1(WebDriver driver) {
		WebElement element = driver.findElement(By.id("ctl00_BodyCph_UpdateProgress1"));
		System.out.println(element.isDisplayed());
		if(element.isDisplayed())
		{
			return true;
		}
		System.out.println("returning false");
		return false;
	}
	

	public static WebElement wait_Explicit(WebDriver driver,String xPath,int sec) {
		
		WebElement element = null;
		try{
		WebDriverWait wait = new WebDriverWait(driver,sec);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPath)));
		} catch(Exception e) {
			// TODO Auto-generated catch block
		    System.out.println("Explicit WAIT Element not clickable");
			e.printStackTrace();
			throw(e);
		}
		return element;
	}

	
	public static void newSkipException() {
		// TODO Auto-generated method stub
			System.out.println("--Inside Skip Exception---");
			throw new SkipException("Skipping the Test---As Data not sufficient for Testing");		
	}
	public static void captureScreenshot(WebDriver driver, String screenshotname) throws Exception {
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source,
					new File("C:\\Work\\Documents\\TestScreenshots\\ScreenShots" + screenshotname + ".png"));
			System.out.println("Screenshot Captured");
		} catch (Exception e) {
			System.out.println("Exception while taking Screenshot" + e.getMessage());
			throw(e);
		}
	}

	public static boolean isAlertPresent(WebDriver driver) {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			return true;
		} catch (NoAlertPresentException ex) {
			return false;
			
		}
	}
	public static void generateMailId(String userId) {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		Date date = new Date();
		System.out.println(userId+formatter.format(date)+"@thrloadtest.in");

	}
	public static void scrollingToBottomofAPage(WebDriver driver) {
		
		 ((JavascriptExecutor) driver)
        .executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

}
