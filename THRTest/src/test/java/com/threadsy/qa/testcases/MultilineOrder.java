package com.threadsy.qa.testcases;


import com.threadsy.qa.utilities.ExtentReport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.threadsy.qa.utilities.TestUtil;
import com.threadsy.qa.utilities.Initial;
import com.threadsy.qa.utilities.ProductsEngine;

public class MultilineOrder extends ExtentReport{

	WebDriver driver;
	public String envi;
	Logger log = Logger.getLogger("testThreadsyLogger");

	@Test(priority = 1, dataProvider = "getProducts")
	public void AddMultipleItemsTest(String Style, String Color, String Size, String Qnty) throws Exception {
		System.out.println("Multi-line Order------Started");
		String[] data = { Style, Color, Size, Qnty };
		ProductsEngine productsEngine = new ProductsEngine();
		productsEngine.startProductExecution(driver, data, "PlaceOrder-1", test);
	}

	@Test(priority = 2)
	public void PlaceOrderTest() throws Exception {

		String[] data = { "804 N Jerry St", "Raymore", "Missouri", "64083-9763", "3333222111", "CC Visa" };
		ProductsEngine productsEngine = new ProductsEngine();
		productsEngine.startProductExecution(driver, data, "PlaceOrder-2", test);
	}
	
	@BeforeTest
	public void beforeTest() throws IOException {
		System.setProperty("webdriver.chrome.driver", "C:\\Work\\Selenium\\Chromedriver_win32 (1)\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://test.threadsy.com/customer/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		Properties prop = Initial.initProperties();
		test = extent.createTest("Multi-line Order Test ");
		test.log(Status.INFO, "Opened Login Page ");
		driver.findElement(By.id(prop.getProperty("User_Id"))).sendKeys(prop.getProperty("userid_admin"));
		test.log(Status.INFO, "Entered UserName ");
		driver.findElement(By.id(prop.getProperty("Password_Id"))).sendKeys(prop.getProperty("pwd_admin"));
		test.log(Status.INFO, "Entered Password");
		// driver.findElement(By.id(prop.getProperty("LoginButton_Id"))).click();
		WebElement ele = driver.findElement(By.id(prop.getProperty("LoginButton_Id")));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].click()", ele);
		System.out.println("Logged in .......");
		test.log(Status.INFO, "Clicked Login button as Customer ");
	}

	@DataProvider
	public Iterator<Object[]> getProducts() {
		ArrayList<Object[]> data = TestUtil.excelReader("sheet2", "ProductsData.xlsx");
		return data.iterator();
	}
	
	@AfterTest
	public void afterTest(){
		driver.quit();
	}

}
