package com.threadsy.qa.testcases;
import com.threadsy.qa.utilities.ExtentManager;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.threadsy.qa.utilities.Initial;
import com.mongodb.diagnostics.logging.Logger;

public class LoginTest {

	WebDriver driver;
	//*public ExtentHtmlReporter extentHtmlReporter;
	public ExtentReports extent= ExtentManager.getInstance("test_default.xls");
	public ExtentTest logger; 
	public String envi;
	
	@Test
	public void loginTest() throws Exception {
		
		Properties prop = Initial.initProperties();
		logger = extent.createTest("Login Test ");
		driver.findElement(By.xpath("//div[@class='header-links__wrapper']/div/span/span[text()='Login']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 4);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='header-links__wrapper']/div/ul/li/a[text()='Sign In']")));
		driver.findElement(By.xpath("//div[@class='header-links__wrapper']/div/ul/li/a[text()='Sign In']")).click();
		logger.log(Status.INFO, "Opened Login Page ");
		//System.out.println("inside LOGIN TEST PArameter--1->>"+param+"----2---->"+param2);
		driver.findElement(By.id(prop.getProperty("User_Id"))).sendKeys(prop.getProperty("userid_admin"));
		logger.log(Status.INFO, "Entered UserName ");
		/*if(param2.equalsIgnoreCase("stage"))
			driver.findElement(By.id(prop.getProperty("Password_Id"))).sendKeys(prop.getProperty("pswd_admin"));
		else if(param2.equalsIgnoreCase("dev"))*/
			driver.findElement(By.id(prop.getProperty("Password_Id"))).sendKeys(prop.getProperty("pwd_admin"));
		logger.log(Status.INFO, "Entered Password");
		driver.findElement(By.id(prop.getProperty("LoginButton_Id"))).click();
		System.out.println("Logged in .......");
		logger.log(Status.INFO, "Clicked Login button as Customer ");
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath(prop.getProperty("Mouse_T-Shirts_xpath")))).build()
				.perform();
		driver.findElement(By.xpath(prop.getProperty("Women_T-shirts_xpath"))).click();
		logger.log(Status.INFO, "Redirected to Categories Page");
		Assert.assertTrue(true);
		//logger.log(LogStatus.PASS,"Logged in Successfully");
/*		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/result.html");
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		ExtentTest logger = extent.createTest("LoginTest");
		logger.log(Status.INFO, "Login Successful");
		extent.flush();*/	
		//driver.quit();
	}
	
	/*@Test
	public void loginFailTest() throws Exception {
		logger = extent.createTest("Login Fail Test ");
		Properties prop = Initial.initProperties();
		logger.log(Status.INFO, "Opened Login Page ");
		driver.findElement(By.id(prop.getProperty("User_Id"))).sendKeys("admin@okok.in");
		logger.log(Status.INFO, "Entered UserName ");
		driver.findElement(By.id(prop.getProperty("Password_Id"))).sendKeys("okok");
		logger.log(Status.INFO, "Entered Password");
		driver.findElement(By.id(prop.getProperty("LoginButton_Id"))).click();
		logger.log(Status.INFO, "Clicked Login Button ");
		System.out.println("Log in ..fail.....");
		//logger.log(LogStatus.INFO, "Log in Failed ");
		Assert.assertTrue(false);
		//driver.quit();
	}
	*/
	@BeforeMethod
	public void beforeMethod(ITestContext context) {
		System.setProperty("webdriver.chrome.driver", "F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://test.threadsy.com/");
		
		/*String fileName = context.getCurrentXmlTest().getParameter("param");
		envi = context.getCurrentXmlTest().getParameter("param2");
		System.out.println("befoore----method---filename--->"+fileName+"--environ-->"+envi);
		if(envi.equalsIgnoreCase("dev"))
			driver.get("http://devims.inoryasoft.com//auth/login");
		else if(envi.equalsIgnoreCase("stage"))
			driver.get("https://stageims.inoryasoft.com/auth/login");*/
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
	}
	
/*	@AfterMethod
	public void afterMethod(ITestResult result) {
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		if (result.getStatus() == ITestResult.FAILURE)
			logger.log(Status.FAIL, result.getThrowable());
        else if (result.getStatus() == ITestResult.SKIP)
        	logger.log(Status.SKIP, result.getThrowable());
        else
        	logger.log(Status.PASS,"Test Passed");
	//	extent.saveTest(logger);
		extent.flush();
		driver.quit();
		
	}*/

	
		
}
