package com.threadsy.qa.testcases;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class TestNGRunner {

	public static void main(String[] args) {
		
		XmlSuite suite = new XmlSuite();
		suite.setName("Threadsy Orders Suite");
		 
		XmlTest test = new XmlTest(suite);
		XmlTest test1 = new XmlTest(suite);
		XmlTest test2 = new XmlTest(suite);
		
		test.setName("Threadsy Single-line Orders Test");		
		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass("com.threadsy.qa.testcases.SingleLineOrder"));
		test.setXmlClasses(classes) ;
		
		test1.setName("Threadsy Multi-line Orders Test");		
		List<XmlClass> class1 = new ArrayList<XmlClass>();		
		class1.add(new XmlClass("com.threadsy.qa.testcases.MultilineOrder"));		
		test1.setXmlClasses(class1) ;
		
		test2.setName("Threadsy Guest Orders Test");		
		List<XmlClass> class2 = new ArrayList<XmlClass>();		
		class2.add(new XmlClass("com.threadsy.qa.testcases.GuestOrder"));		
		test2.setXmlClasses(class2) ;
		
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		tng.setOutputDirectory(System.getProperty("user.dir")+"/THRTest/test-output");
		tng.run();
		
		}
	

}
