package com.threadsy.qa.testcases;
import com.threadsy.qa.utilities.ExtentManager;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.threadsy.qa.utilities.Initial;



import org.openqa.selenium.Keys;

public class SearchPDP {

	WebDriver driver;
	//*public ExtentHtmlReporter extentHtmlReporter;
	public ExtentReports extent= ExtentManager.getInstance("test_default.xls");
	public ExtentTest logger; 
	public String envi;
	
	@Test
	public void SearchTest() throws Exception {
		
		Properties prop = Initial.initProperties();
		logger = extent.createTest("Multi-line Order Test ");
		logger.log(Status.INFO, "Opened Home Page ");
		driver.findElement(By.id(prop.getProperty("User_Id"))).sendKeys(prop.getProperty("userid_admin"));
		logger.log(Status.INFO, "Entered UserName ");
		driver.findElement(By.id(prop.getProperty("Password_Id"))).sendKeys(prop.getProperty("pwd_admin"));
		logger.log(Status.INFO, "Entered Password");
		driver.findElement(By.id(prop.getProperty("LoginButton_Id"))).click();
		System.out.println("Logged in .......");
		logger.log(Status.INFO, "Clicked Login button as Customer ");
		driver.findElement(By.id(prop.getProperty("SearchID"))).sendKeys(prop.getProperty("ProductStyle"));
		logger.log(Status.INFO, "Entered Product to search ");
		driver.findElement(By.id(prop.getProperty("SearchID"))).sendKeys(Keys.ENTER);
	
		boolean isElemPresent = (driver.findElements(By.xpath("//div[text()[contains(.,' 370J ')]]/ancestor::div[@class='product-item-info']/a/span/span/img")).size() != 0);
		if(!isElemPresent){
			while(!(driver.findElements(By.xpath("//div[text()[contains(.,' 370J ')]]/ancestor::div[@class='product-item-info']/a/span/span/img")).size() != 0))
			{
				System.out.println("Pagination .......");
			if(driver.findElements(By.xpath("(//div[@class='pages']/ul[@class='items pages-items']/li/a[@title='Next'])[2]")).size() != 0)
				driver.findElement(By.xpath("(//div[@class='pages']/ul[@class='items pages-items']/li/a[@title='Next'])[2]")).click();
			else{
				System.out.println("--Terminating the Test--- Skip Exception---");
				throw new SkipException("Skipping the Test---As Product is not avaiable in search for Testing");
			}
			isElemPresent = (driver.findElements(By.xpath("//div[text()[contains(.,' 370J ')]]/ancestor::div[@class='product-item-info']/a/span/span/img")).size() != 0);
		}
		}
		driver.findElement(By.xpath("//div[text()[contains(.,' 370J ')]]/ancestor::div[@class='product-item-info']/a/span/span/img")).click();
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;  
		WebElement eleColor =driver.findElement(By.xpath("//div[@class='btn-outline']"));
		//jsExecutor.executeScript("arguments[0].scrollIntoView(true);", eleColor);	
		//jsExecutor.executeScript("arguments[0].click();",eleColor);
		Actions action = new Actions(driver);		
	//	action.moveToElement(eleColor).click().perform();		
	//	driver.findElement(By.xpath("//div[@class='btn-outline']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/div[@class='btn-outline']")));
		WebElement moreColors = driver.findElement(By.xpath("//div/div[@class='btn-outline']"));
		jsExecutor.executeScript("arguments[0].click();",moreColors);
		WebElement eleQty =driver.findElement(By.xpath("//div[text()='CLASSIC DENIM']/ancestor::tr/th/div[text()='XL']/following-sibling::input"));
			jsExecutor.executeScript("arguments[0].value='3'",eleQty); 
		WebElement eleAdd =driver.findElement(By.xpath("//div[@class='actions']/button[@id='product-addtocart-bulk-button']"));
		//form[@id='product_addtocart_bulk_form']//div[@class='actions']
		//jsExecutor.executeScript("arguments[0].click();",eleAdd);
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", eleAdd);	
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='actions']/button[@id='product-addtocart-bulk-button']")));
		action.moveToElement(eleAdd).build().perform();
		action.moveToElement(eleAdd).click().perform();
		//driver.findElement(By.xpath("//a/span/span/span[text()[contains(.,'Cart')]]")).click();
		//Assert.assertTrue(true);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'shopping cart')]]")));
		driver.findElement(By.xpath("//a[text()[contains(.,'shopping cart')]]")).click();
		System.out.println("Added to Cart.......");
	//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li/button[@data-role='proceed-to-checkout']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//li/button[@data-role='proceed-to-checkout']")).click();
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='new-address-popup']/button")));
		WebElement addAddress = driver.findElement(By.xpath("//div[@class='new-address-popup']/button"));
		jsExecutor.executeScript("arguments[0].click();",addAddress);
		driver.findElement(By.xpath("//input[@name='street[0]']")).sendKeys("804 N Jerry St");
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Raymore");
		Select stateEle = new Select(driver.findElement(By.xpath("//select[@name='region_id']")));
		stateEle.selectByVisibleText("Missouri");
		driver.findElement(By.xpath("//input[@name='postcode']")).sendKeys("64083-9763");
		driver.findElement(By.xpath("//input[@name='telephone']")).sendKeys("3333222111");
		driver.findElement(By.xpath("//button[@class='action primary action-save-address']")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div/input[@id='s_method_mptablerate_1']/following-sibling::label")));
		//driver.findElement(By.xpath("//div[@id='label_method_1_mptablerate']")).click();
		//div[@id='label_method_1_mptablerate' and text()[contains(.,'Standard Shipping')]]
		WebElement eleShipping = driver.findElement(By.xpath("//div/input[@id='s_method_mptablerate_1']/following-sibling::label"));
	//	action.moveToElement(eleShipping).build().perform();
		//action.moveToElement(eleShipping).click().perform();
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", eleShipping);
		jsExecutor.executeScript("arguments[0].click();",eleShipping);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-role='opc-continue']")));
		WebElement eleNext=driver.findElement(By.xpath("//button[@data-role='opc-continue']"));
		action.moveToElement(eleNext).build().perform();
		action.moveToElement(eleNext).click().perform();
		Thread.sleep(3000);
	//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='stripe-payments-saved-card']/input[@name='payment[cc_saved]']")));
		WebElement eleCard =driver.findElement(By.xpath("//span[@class='exp stripe-payments-fade']"));
		action.moveToElement(eleCard).build().perform();
		action.moveToElement(eleCard).click().perform();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='root']/form/span[2]/div/div[2]/span/input[@name='cardnumber' and @placeholder='1234 1234 1234 1234']")));
		//driver.findElement(By.xpath("//input[@name='cardnumber' and @placeholder='1234 1234 1234 1234']")).sendKeys(prop.getProperty("validCardNumber"));
	/*	WebElement cardNo =driver.findElement(By.xpath("//*[@id='root']/form/span[2]/div/div[2]/span/input[@name='cardnumber' and @placeholder='1234 1234 1234 1234']"));
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", cardNo);
		jsExecutor.executeScript("arguments[0].value='4111 1111 1111 1111'",cardNo); 
		driver.findElement(By.xpath("//input[@name='cc-exp-month']")).sendKeys(prop.getProperty("cardExpMonth/Year"));
		driver.findElement(By.xpath("//input[@name='cvc']")).sendKeys(prop.getProperty("cvvNo"));*/
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='payment[cc_saved]' and not(contains(@id,'new_card'))]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@title='Place Order']")).click();
		//driver.quit();
	}
	
	
	@BeforeMethod
	public void beforeMethod(ITestContext context) {
		System.setProperty("webdriver.chrome.driver", "F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://test.threadsy.com/customer/account/login");
			
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
				
	}
	
	/*@AfterMethod
	public void afterMethod(ITestResult result) {
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		if (result.getStatus() == ITestResult.FAILURE)
			logger.log(Status.FAIL, result.getThrowable());
        else if (result.getStatus() == ITestResult.SKIP)
        	logger.log(Status.SKIP, result.getThrowable());
        else
        	logger.log(Status.PASS,"Test Passed");
	//	extent.saveTest(logger);
		extent.flush();
		driver.quit();		
	}*/

	
		
}
