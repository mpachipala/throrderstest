package com.threadsy.qa.testcases;


import com.threadsy.qa.utilities.ExtentReport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.threadsy.qa.utilities.TestUtil;
import com.threadsy.qa.utilities.Initial;
import com.threadsy.qa.utilities.ProductsEngine;

public class SingleLineOrder extends ExtentReport {

	WebDriver driver;
	public String envi;
	Logger log = Logger.getLogger("testThreadsytest");

	@Test(dataProvider = "getRegProducts")
	public void SingleOrderTest(String Style, String Color, String Size, String Qnty) throws Exception {
		System.out.println("Single-line Order------Started");
		Properties prop = Initial.initProperties();
		test = extent.createTest("Single-Line Order Test ");
		test.log(Status.INFO, "Opened Login Page ");
		log.debug("Placing Order for Customer details");
		test.log(Status.INFO, "Clicked Login button as Customer ");

		String[] data = { prop.getProperty("userid_admin"), prop.getProperty("pwd_admin"), Style, Color, Size, Qnty,
				"804 N Jerry St", "Raymore", "Missouri", "64083-9763", "3333222111" };
		ProductsEngine productsEngine = new ProductsEngine();
		productsEngine.startProductExecution(driver, data, "PlaceOrder", test);

	}
	
	@BeforeTest
	public void beforeMethod(ITestContext context) {
		System.setProperty("webdriver.chrome.driver", "C:\\Work\\Selenium\\Chromedriver_win32 (1)\\chromedriver.exe");
		driver = new ChromeDriver();
		envi = context.getCurrentXmlTest().getParameter("param");
		/*System.out.println("before----method-----environ-->" + envi);
		if (envi == null)
			envi = "dev";
		if (envi.equalsIgnoreCase("dev"))
			driver.get("https://test.threadsy.com/customer/account/login");
		else if (envi.equalsIgnoreCase("live"))*/
			driver.get("https://test.threadsy.com/customer/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);

	}

	@DataProvider
	public Iterator<Object[]> getRegProducts() {
		ArrayList<Object[]> data = TestUtil.excelReader("sheet1", "ProductsData.xlsx");
		return data.iterator();
	}
	
	@AfterTest
	public void afterTest(){
		driver.quit();
	}

}
